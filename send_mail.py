#!/usr/bin/env python3
import httplib2
import os
import oauth2client
from oauth2client import client, tools, file
import base64
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from googleapiclient import discovery
import re
from datetime import date
class Mail:
    def __init__(self):
        self.mail_from = "production.notifications@carrefour.com"
        self.mail_cred_path = "./cred.json"
        self.user_id = "me"
    def oecparam(self):
        self.mail_param = {}
        self.mail_param["mailTo"] = "renish.joseph@mindcurv.com"
        self.mail_param["mailTitle"] = "Prod Couchbase and MongoDB : SYSLOG Error Summary " + date.today().strftime("%d/%m/%y")
        self.mail_param["mailDescription"] = open('message.txt', 'r').read()
        return self.mail_param
    def send(self, param):
        try:
            store = oauth2client.file.Storage(self.mail_cred_path)
            credentials = store.get()
            if not credentials or credentials.invalid:
                print("Gmail API token genererated is invalid. Check and generate valid token again.")
                return "noVal"
            else:
                http = credentials.authorize(httplib2.Http())
                service = discovery.build('gmail', 'v1', http=http, cache_discovery=False)
                message = MIMEText(param["mailDescription"])
                message['to'] = param["mailTo"]
                message['from'] = self.mail_from
                message['subject'] = param["mailTitle"]
                raw_message = base64.urlsafe_b64encode(message.as_string().encode("utf-8"))
                message_decoded = { 'raw': raw_message.decode("utf-8") }
                mail_send = service.users().messages().send(userId=self.user_id, body=message_decoded).execute()
                print('Message: %s' % message)
                return mail_send['id']
        except Exception as e:
            print('An error occurred: %s' % e)
            return "noVal"
def main():
    mailss = Mail()
    param = mailss.oecparam()
    mailss.send(param)
if __name__ == '__main__':
    main()
